package com.teccart.pchar.diceapi23;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;

import java.util.Random;

import static android.media.MediaPlayer.create;


public class Main3Activity extends AppCompatActivity {

    private int nbJeux;
    private static int nbTours=0;
    private static int PlayerID=2;
    public int[] PlayerScores= new int[2];

    private String bob;
    private TextView t;
    private TextView t1;
    private TextView t2;
    private ImageView topDicePic;
    private ImageView BottomDicePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        PlayerScores[0]=0;
        PlayerScores[1]=0;

        Bundle b = getIntent().getExtras();

        nbJeux=Integer.parseInt(b.getString("nbJeux"))*2;
        t =(TextView)findViewById(R.id.textViewPlayer);
        t1 =(TextView)findViewById(R.id.textViewPlayer1);
        t2 =(TextView)findViewById(R.id.textViewPlayer2);

        topDicePic=(ImageView)findViewById(R.id.imageViewTopDice);
        BottomDicePic=(ImageView)findViewById(R.id.imageViewBottomDice);
        //initialize picture if we dont want first roll to be empty pics
        topDicePic.setImageResource(R.drawable.de1);
        BottomDicePic.setImageResource(R.drawable.de1);
        Button q=(Button)findViewById(R.id.btnRoll);

        Random rand = new Random();
        PlayerID = rand.nextInt(2); // Gives n such that 0 <= n < 5
        t.setText("Player "+ (PlayerID+1) +", it is your turn");
        t1.setText("Player 1: "+ PlayerScores[0]);
        t2.setText("Player 2: "+ PlayerScores[1]);



        /*do{
            unTour();
            nbTours++;
        }while(nbTours!=nbJeux);*/
    }




    public void RollDices(View view) {

        int score=0;



        MediaPlayer mPlayer2;
        mPlayer2= create(this, R.raw.rollingdicecut);
        mPlayer2.start();

        //Top dice
        Random rand = new Random();
        final int n = rand.nextInt(6)+1; // Gives n such that 0 <= n < 6

        score=n;

        //Bottom Dice

        final int n2 = rand.nextInt(6)+1; // Gives n such that 0 <= n < 5

        score+=n2;

        addScore(PlayerScores, score);


        Runnable orderOfThings = new Runnable() {
            public void run() {
                changeImv(topDicePic, n);
                changeImv(BottomDicePic, n2);
            }
        };
        topDicePic.postOnAnimationDelayed(orderOfThings, 1750);


        nbTours++;



        if(nbTours==nbJeux)
        {
            GameOver();
        }
    }

    public void addScore(int[] Scores, int ValueToAdd)
    {
        topDicePic.animate().rotation(topDicePic.getRotation() + 720f).setDuration(2000);
        BottomDicePic.animate().rotation(BottomDicePic.getRotation() + 720f).setDuration(2000);


        Scores[PlayerID]+=ValueToAdd;

        if(PlayerID==0)
            PlayerID=1;
        else if (PlayerID==1)
            PlayerID=0;
        else
        {
            Context context = getApplicationContext();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle("Your Title");

            builder.setMessage("Some message...")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // TODO: handle the OK
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        t.setText("Player "+ (PlayerID+1) +", it is your turn");
        t1.setText("Player 1: "+ PlayerScores[0]);
        t2.setText("Player 2: "+ PlayerScores[1]);

    }

    public void GameOver()
    {

        /*Context context = getApplicationContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context,android.R.style.Theme_Material_Dialog_Alert);

        builder.setTitle("The game has finished!");*/

        //Player 1 wins
        if(PlayerScores[0]>PlayerScores[1])
        {
            CreateAlertDialog();
            t1.setTextSize(30f);
        }
        //Player 2 wins
        else if(PlayerScores[0]<PlayerScores[1])
        {
            CreateAlertDialog();
            t2.setTextSize(30f);
        }
        //in case of a tie or anything else
        else
        {
            CreateAlertDialog();
        }

    }
    public void CreateAlertDialog()
    {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

// 2. Chain together various setter methods to set the dialog characteristics
        if(PlayerScores[0]>PlayerScores[1])
        {
            builder.setMessage("Player 1 is victorious")
                    .setTitle("Game has finished");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent Facteur = new Intent(Main3Activity.this,MainActivity.class);
                    startActivity(Facteur);
                }
            });
        }
        //Player 2 wins
        else if(PlayerScores[0]<PlayerScores[1])
        {
            builder.setMessage("Player 2 is victorious")
                    .setTitle("Game has finished");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent Facteur = new Intent(Main3Activity.this,MainActivity.class);
                    startActivity(Facteur);
                }
            });
        }
        //in case of a tie or anything else
        else
        {
            builder.setMessage("The result is a tie.")
                    .setTitle("Game has finished");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent Facteur = new Intent(Main3Activity.this,MainActivity.class);
                    startActivity(Facteur);
                }
            });
        }


// 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void changeImv(ImageView imvDemo, int n)
    {
        if(n==1)
            imvDemo.setImageResource(R.drawable.de1);
        if(n==2)
            imvDemo.setImageResource(R.drawable.de2);
        if(n==3)
            imvDemo.setImageResource(R.drawable.de3);
        if(n==4)
            imvDemo.setImageResource(R.drawable.de4);
        if(n==5)
            imvDemo.setImageResource(R.drawable.de5);
        if(n==6)
            imvDemo.setImageResource(R.drawable.de6);
    }
}
