package com.teccart.pchar.diceapi23;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {

    private EditText nbJeux;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        nbJeux =(EditText)findViewById(R.id.nbJeux);
    }

    public void BeginGame(View view) {

        if(Integer.parseInt(nbJeux.getText().toString())>=1 && Integer.parseInt(nbJeux.getText().toString())<=10)
        {
            Intent Facteur2 = new Intent(this,Main3Activity.class);
            Facteur2.putExtra("nbJeux",nbJeux.getText().toString());
            startActivity(Facteur2);
        }
        else
        {
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please select a number of turns between 1 and 10")
                    .setTitle("Incorrect Number of Turns");
            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }
}